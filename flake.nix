{
  description = "Mostly games I guess";

  inputs = {
    # Nixpkgs from right before the move from Godot 3.4.4 -> 3.5
    nixpkgs-godot-3_4.url = github:NixOS/nixpkgs/?ref=cfe12e8c8a43a5fe5d8c13cdb97cb2ab0a20c847;
  };

  outputs = { self, nixpkgs, nixpkgs-godot-3_4 }: {

    packages.x86_64-linux.tabletop-club-bin = with nixpkgs.legacyPackages.x86_64-linux;
      stdenv.mkDerivation rec {
        pname = "TabletopClub.x86_64";
        version = "0.1.0";

        src = fetchzip {
          url = "https://github.com/drwhut/tabletop-club/releases/download/v${version}/TabletopClub_v${version}_Linux_64.zip";
          sha256 = "sha256-WNWQ2Rdtqth5kIhRikzvaCmhD4aO321RRYKVsoAHIh4=";
          stripRoot = false;
        };

        nativeBuildInputs = [
          autoPatchelfHook
        ];

        buildInputs = [
          libfakeXinerama.out
          libglvnd.out
          xorg.libX11.out
          xorg.libXcursor.out
          xorg.libXext.out
          xorg.libXi.out
          xorg.libXrandr.out
          xorg.libXrender.out
          gcc-unwrapped.lib
        ];

        sourceRoot = ".";

        installPhase = ''
          install -m755 -D source/TabletopClub.x86_64 -t $out/bin/
          cp source/{TabletopClub.pck,libwebrtc_native.linux.release.64.so} $out/bin/
          mkdir $out/Resources
          cp -r source/assets $out/Resources/
        '';

        meta = with lib; {
          homepage = "https://drwhut.itch.io/tabletop-club";
          description = "Tabletop game simulator";
          platforms = platforms.linux;
        };
      };

    packages.x86_64-linux.godot-tabletop = with nixpkgs.legacyPackages.x86_64-linux; godot.overrideAttrs (attrs:
      rec {
        version = "tabletop-3.4.5";

        src = fetchFromGitHub {
          owner = "drwhut";
          repo = "godot";
          rev = "0235ebdf7c05f43a206a41b063b6ca6ce3a5e10e"; # Branch tabletop-3.4.5 as of 2023-05-06
          sha256 = "sha256-o40W02WvgTxT/7RJzJvLVKa2QkFR3oeTZv+5/wf5GEQ=";
          fetchSubmodules = true;
        };
      }
    );

    packages.x86_64-linux.godot-tabletop-headless = with nixpkgs.legacyPackages.x86_64-linux;
      godot-headless.override {
        godot = self.packages.x86_64-linux.godot-tabletop;
      };

    packages.x86_64-linux.tabletop-club = with nixpkgs.legacyPackages.x86_64-linux;
      callPackage ./games/tabletop-club.nix {
        godot-tabletop = self.packages.x86_64-linux.godot-tabletop;
        # We need a headless version of Godot (that does not attempt to access the X11 server)
        # to export the .pck file in the nix builder. That version needs to be compatible
        # with the version of Godot that will read the .pck.
        # Leveraging cache.nixos.org lets us avoid recompiling a headless version of
        # the Tabletop Club fork
        godot-headless = nixpkgs-godot-3_4.legacyPackages.x86_64-linux.godot-headless;
      };

    packages.x86_64-linux.default = self.packages.x86_64-linux.tabletop-club;

    devShells.x86_64-linux.default = with nixpkgs.legacyPackages.x86_64-linux; mkShell {
      buildInputs = [
        nixpkgs-fmt
      ];
    };
  };
}
