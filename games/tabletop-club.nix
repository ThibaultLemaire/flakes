# Derived from nixpkgs/pkgs/applications/editors/pixelorama/default.nix @ e4474334415ac41efb5fda33d4cc8f312397ef05

{ lib
, stdenv
, fetchFromGitHub
, godot-tabletop
, godot-headless
, writeTextFile
, fetchzip
, gcc-unwrapped
}:

let
  preset =
    "Linux/X11";
  export-presets = writeTextFile {
    name = "export_presets.cfg";
    text = ''
      [preset.0]

      name="${preset}"
      platform="Linux/X11"
      runnable=true
      custom_features=""
      export_filter="all_resources"
      include_filter=""
      exclude_filter=""
      export_path="./."
      script_export_mode=1
      script_encryption_key=""

      [preset.0.options]

      custom_template/debug=""
      custom_template/release="${godot-tabletop}/bin/.godot-wrapped"
      binary_format/64_bits=true
      binary_format/embed_pck=false
      texture_format/bptc=false
      texture_format/s3tc=true
      texture_format/etc=false
      texture_format/etc2=false
      texture_format/no_bptc_fallbacks=true

    '';
  };
  # Compiling the dreaded thing from source looks like a nightmare,
  # so just grab a pre-compiled bin, patch it, and call it a day
  godot-webrtc-native-plugin-bin = (version: fetchzip {
    url = "https://github.com/godotengine/webrtc-native/releases/download/${version}/godot-webrtc-native-release-${version}.zip";
    sha256 = "sha256-LR0udMfa++J4uuaaRoG+pNJB/1pwbjJp3KFpUkiXr0c=";
    stripRoot = false;
  }) "0.5";
  rpaths = lib.makeLibraryPath [ gcc-unwrapped.lib ];
in
stdenv.mkDerivation
rec {
  pname = "tabletop-club";
  version = "0.1.0";

  src = fetchFromGitHub {
    owner = "drwhut";
    repo = pname;
    rev = "v${version}";
    sha256 = "sha256-6hHjGsUKIkZtR6cPianoNy2eoinDTfbA4Akizr185Ps=";
  };

  nativeBuildInputs = [
    godot-headless
  ];

  buildPhase = ''
    runHook preBuild

    # Godot puts its cache there (but we know that's going to be a miss and never used...)
    export HOME=$(mktemp -d)
    mkdir -p $HOME/.local/share/godot/

    cd game/

    ln -sv "${export-presets}" ./export_presets.cfg
    ln -sv "${godot-webrtc-native-plugin-bin}"/webrtc ./webrtc

    mkdir -p build
    godot-headless -v --export "${preset}" ./build/${pname}
    godot-headless -v --export-pack "${preset}" ./build/${pname}.pck

    cd -

    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall

    install -D -m 755 -t $out/libexec ./game/build/${pname}
    install -D -m 644 -t $out/libexec ./game/build/${pname}.pck
    install -D -m 644 -t $out/libexec ./game/build/libwebrtc_native.linux.release.64.so
    patchelf --add-rpath "${rpaths}" $out/libexec/libwebrtc_native.linux.release.64.so
    install -d -m 755 $out/Resources
    cp -r assets/ $out/Resources/
    install -d -m 755 $out/bin
    ln -s $out/libexec/${pname} $out/bin/${pname}

    runHook postInstall
  '';

  meta = with lib; {
    homepage = "https://drwhut.itch.io/tabletop-club";
    description = "An open-source platform for playing tabletop games in a physics-based 3D environment for Windows, macOS, and Linux! Made with the Godot Engine";
    license = licenses.mit;
    platforms = [ "x86_64-linux" ];
    maintainers = with maintainers; [ ];
  };
}
